//ROUTE FUNCTIONS
var getErrorUrl = function(params) {
    return 'error/'+params.errorNum+'.html';
};

// Angular

angular.module('loadTestApp', ['ngRoute', 'ngSanitize', 'ngCookies', 'ngAnimate', 'ui.bootstrap', 'loadTestApp.services'])
	.config(
		[ '$routeProvider', function($routeProvider) {

			$routeProvider.when('/error/:errorNum', {
				templateUrl: getErrorUrl,
				controller: ErrorController
			});

            $routeProvider.when('/create', {
                templateUrl: 'widgets/createRun.html',
                controller: CreateTestController
            });

            $routeProvider.when('/report/:runNum', {
                templateUrl: 'widgets/runReport.html',
                controller: RunReportController
            });

			//Default			
			$routeProvider.otherwise({
				templateUrl: 'widgets/dashboard.html',
				controller: DashboardController
			});

		} 
	]) // end config
	
	//Filters
	.filter('iif', function () {
		   return function(input, trueValue, falseValue) {
		        return input ? trueValue : falseValue;
		   };
	})

	.run(function($rootScope) {
		/* Reset error when a new view is loaded */
		$rootScope.$on('$viewContentLoaded', function() {
			delete $rootScope.msg;
			delete $rootScope.error;
		});
		
		$rootScope.currentYear = new Date().getFullYear();
		$rootScope.initialized = true;
	}
); //end of run

//CONTROLLERS

function CreateTestController($rootScope, $scope, $location) {
    $scope.ajaxSent = false;
    $scope.testRepetitions = 1;
    $scope.virtualUsers = 10;

    $scope.cbLogic = function(caller) {
        if ('persist' === caller && ! $("#cbPersistResponse").prop("checked")) {
            $("#cbTestResponse").prop( "checked", false);
        }
        if ('test' === caller &&  $("#cbTestResponse").prop("checked")) {
            $("#cbPersistResponse").prop( "checked", true);
        }
    };

    /**
     * AngularJS is having difficulties with files. Just use HTML5.
     */
    $scope.createRun = function() {
        $scope.ajaxSent = true;
        var formData = new FormData();
        formData.append('postmanFile', document.getElementById('postmanFile').files[0]);
        formData.append('envFile', document.getElementById('envFile').files[0]);
        formData.append('virtualUsers', $scope.virtualUsers);
        formData.append('testRepetitions', $scope.testRepetitions);
        formData.append('persistResponse', $scope.persistResponse);
        formData.append('testResponse', $scope.testResponse);

        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (this.status === 200) {
                var testPath = '/report/' + this.responseText;
                // Doesn't work for some reason.
                $location.url(testPath);
                window.location = '/#' + testPath;
            } else {
                $rootScope.error = this.response;
                $scope.ajaxSent = false;
            }
        };
        xhr.open("POST", '/test');
        xhr.send(formData);
    }
}

function DashboardController($scope, TestService) {
    $scope.testRuns = TestService.query();
}

function ErrorController($scope, $rootScope) {
	$scope.errorDetails = $rootScope.error;
	delete $rootScope.error;
}

function RunReportController($scope, $routeParams, $timeout, TestService) {
    $scope.showTestRunning = false;
    $scope.loadTestId = $routeParams.runNum;
    $scope.refreshSeconds = 5;
    load();

    function load() {
        TestService.get({id: $routeParams.runNum}, function(loadTest) {
            $scope.loadTest = loadTest;
            if (!loadTest.complete) {
                $scope.showTestRunning = true;
               if ($scope.refreshSeconds > 0) {
                    $timeout(function () {
                        load();
                    }, $scope.refreshSeconds * 1000);
               }
            } else {
                $scope.showTestRunning = false;
            }
        });
    }
}

//SERVICES
var services = angular.module('loadTestApp.services', ['ngResource']);

services.factory('TestService', function($resource) {
	return $resource('test/:id/', {}, {});
});
