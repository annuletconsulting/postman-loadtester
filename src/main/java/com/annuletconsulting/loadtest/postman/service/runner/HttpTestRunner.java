package com.annuletconsulting.loadtest.postman.service.runner;

import com.annuletconsulting.loadtest.postman.util.HttpExceptionResponse;
import com.annuletconsulting.loadtest.postman.model.db.ResponseData;
import com.annuletconsulting.loadtest.postman.model.db.RequestData;
import com.annuletconsulting.loadtest.postman.model.db.ResponseDataDao;
import com.annuletconsulting.loadtest.postman.model.db.TestRun;
import com.annuletconsulting.loadtest.postman.model.json.Value;
import com.annuletconsulting.loadtest.postman.util.PostmanUtil;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by walter.moorhouse on 2/7/17.
 */
public class HttpTestRunner implements Runnable {
    private static final String VIRTUAL_USER = "Virtual User ";

    private final int testRepetitions;
    private final int id;
    private final List<RequestData> requestData;
    private final ResponseDataDao responseDataDao;
    private final TestRun testRun;

    public HttpTestRunner(int id, TestRun testRun, List<RequestData> requestData,
                          int testRepetitions, ResponseDataDao responseDataDao) {
        this.id = id;
        this.testRun = testRun;
        this.testRepetitions = testRepetitions;
        this.requestData = requestData;
        this.responseDataDao = responseDataDao;
    }

    @Override
    public void run() {
        System.out.println("Starting Virtual User: "+id);
        for (int x = 1; x<= testRepetitions; x++) {
            for (RequestData requestDatum : requestData) {
                HttpClient client = HttpClientBuilder.create().build();
                HttpRequestBase request = new HttpGet();
                if ("GET".equals(requestDatum.getMethod())) {
                    request = new HttpGet(requestDatum.getUrl());
                } else if ("POST".equals(requestDatum.getMethod())) {
                    request = new HttpPost(requestDatum.getUrl());
                    addPostBody(request, requestDatum);
                }
                // add request header
                for (Map.Entry<String, String> entry : requestDatum.getHeaderMap().entrySet()) {
                    request.addHeader(entry.getKey(), entry.getValue());
                }

                //Run tests, with timing.
                Calendar testBegin = Calendar.getInstance();
                HttpResponse response = getResponse(client, request);
                Calendar testEnd = Calendar.getInstance();

                int statusCode = response.getStatusLine().getStatusCode();
                String responseBody = null;
                if (HttpStatus.SC_OK == statusCode && testRun.isResponsePersisted()) {
                    responseBody = getResponseBody(response);
                }
                responseDataDao.save(new ResponseData(id, x, testRun, requestDatum,
                        testBegin, testEnd, statusCode, response.getAllHeaders(), responseBody));
            }

            System.out.println(new StringBuilder().append(VIRTUAL_USER).append(id).append(" completed repetition ")
                    .append(x).append('/').append(testRepetitions).append('.').toString());
        }
        if (testRun.isResponseTested()) {
            System.out.println(new StringBuilder().append(VIRTUAL_USER).append(id)
                    .append(" started testing responses.").toString());
            PostmanUtil.runPostmanTests(requestData, responseDataDao);
        }
        System.out.println(new StringBuilder().append(VIRTUAL_USER).append(id).append(" done.").toString());
    }

    private static void addPostBody(HttpRequestBase request, RequestData requestDatum) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        if (null != requestDatum.getFormData()) {
            for (Value formdata : requestDatum.getFormData()) {
                if (formdata.isEnabled()) {
                    builder.addTextBody(formdata.getKey(), formdata.getValue(), ContentType.TEXT_PLAIN);
                }
            }
        }
        if (null != requestDatum.getFile()) {
            File file = new File(requestDatum.getFile());
            try (FileInputStream fileIS = new FileInputStream(file)) {
                builder.addBinaryBody("file",
                        fileIS,
                        ContentType.APPLICATION_OCTET_STREAM,
                        file.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ((HttpPost) request).setEntity(builder.build());
    }

    private static String getResponseBody(HttpResponse response) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
            String tmp;
            StringBuilder stringBuilder = new StringBuilder();
            while ( null != (tmp = reader.readLine()) ) {
                stringBuilder.append(tmp);
            }
            return stringBuilder.toString();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return "Tester Exception reading response body: " + ioe.getLocalizedMessage();
        }
    }

    private static HttpResponse getResponse(HttpClient client, HttpRequestBase request) {
        HttpResponse response;
        try {
            response = client.execute(request);
        } catch (IOException ioe) {
            response = new HttpExceptionResponse(ioe,
                    request.getProtocolVersion().getProtocol(),
                    request.getProtocolVersion().getMajor(),
                    request.getProtocolVersion().getMinor());
        }
        return response;
    }
}
