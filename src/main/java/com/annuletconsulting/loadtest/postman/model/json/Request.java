
package com.annuletconsulting.loadtest.postman.model.json;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "auth",
    "url",
    "method",
    "header",
    "body",
    "description"
})
public class Request {

    @JsonProperty("auth")
    @Valid
    private com.annuletconsulting.loadtest.postman.model.json.Auth auth;

    @JsonProperty("url")
    private String url;
    @JsonProperty("method")
    private String method;
    @JsonProperty("header")
    private List<Header> header = new ArrayList<Header>();
    @JsonProperty("body")
    private Body body;
    @JsonProperty("description")
    private String description;

    /**
     *
     * @return
     *     The auth
     */
    @JsonProperty("auth")
    public com.annuletconsulting.loadtest.postman.model.json.Auth getAuth() {
        return auth;
    }

    /**
     *
     * @param auth
     *     The auth
     */
    @JsonProperty("auth")
    public void setAuth(com.annuletconsulting.loadtest.postman.model.json.Auth auth) {
        this.auth = auth;
    }

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The method
     */
    @JsonProperty("method")
    public String getMethod() {
        return method;
    }

    /**
     * 
     * @param method
     *     The method
     */
    @JsonProperty("method")
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * 
     * @return
     *     The header
     */
    @JsonProperty("header")
    public List<Header> getHeader() {
        return header;
    }

    /**
     * 
     * @param header
     *     The header
     */
    @JsonProperty("header")
    public void setHeader(List<Header> header) {
        this.header = header;
    }

    /**
     * 
     * @return
     *     The body
     */
    @JsonProperty("body")
    public Body getBody() {
        return body;
    }

    /**
     * 
     * @param body
     *     The body
     */
    @JsonProperty("body")
    public void setBody(Body body) {
        this.body = body;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

}
