package com.annuletconsulting.loadtest.postman.model.js;

/**
 * Created by walt on 4/9/17.
 */
public class ResponseCode {
    private int code;
    private String status;

    public ResponseCode(int code, String status) {
        this.code = code;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
