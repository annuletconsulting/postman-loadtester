package com.annuletconsulting.loadtest.postman.model.db;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by walt on 2/25/17.
 */
@Transactional
public interface TestRunDao extends CrudRepository<TestRun, Long> {
}
