package com.annuletconsulting.loadtest.postman.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.http.Header;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by walt on 2/24/17.
 */
@Entity
@Table(name = "response_data")
public class ResponseData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="run_id", nullable = false)
    private TestRun testRun;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="request_id", nullable = false)
    private RequestData requestData;

    @Column(name="test_begin", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar testBegin;

    @Column(name="test_end", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar testEnd;

    @Column(name="virtual_user", nullable = false)
    private Integer virtualUser;

    @Column(name="repitition_num", nullable = false)
    private Integer repetitionNumber;

    @Column(name="status_code", nullable = false)
    private Integer httpStatusCode;

    @JsonInclude
    @Lob
    @Column(name = "response_body")
    private String responseBody;

    @Column(name = "test_results")
    private String testResults;

    @Column(name = "response_headers")
    private String allHeaders;

    public ResponseData() {
        // Needed for hibernate.
    }

    public ResponseData(Integer virtualUser, Integer repetitionNumber, TestRun testRun, RequestData requestData,
                        Calendar testBegin, Calendar testEnd, Integer httpStatusCode, Header[] allHeaders,
                        String responseBody) {
        this.virtualUser = virtualUser;
        this.repetitionNumber = repetitionNumber;
        this.testRun = testRun;
        this.requestData = requestData;
        this.testBegin = testBegin;
        this.testEnd = testEnd;
        this.httpStatusCode = httpStatusCode;
        this.responseBody = responseBody;
        StringBuilder headers = new StringBuilder();
        for (Header header : allHeaders) {
            headers.append(header.getName()).append('=').append(header.getValue()).append(';');
        }
        this.allHeaders = headers.substring(0, headers.length()==0?0:headers.length()-1);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestRun getTestRun() {
        return testRun;
    }

    public void setTestRun(TestRun testRun) {
        this.testRun = testRun;
    }

    public RequestData getRequestData() {
        return requestData;
    }

    public void setRequestData(RequestData requestData) {
        this.requestData = requestData;
    }

    public Calendar getTestBegin() {
        return testBegin;
    }

    public void setTestBegin(Calendar testBegin) {
        this.testBegin = testBegin;
    }

    public Calendar getTestEnd() {
        return testEnd;
    }

    public void setTestEnd(Calendar testEnd) {
        this.testEnd = testEnd;
    }

    public Integer getRepetitionNumber() {
        return repetitionNumber;
    }

    public void setRepetitionNumber(Integer repetitionNumber) {
        this.repetitionNumber = repetitionNumber;
    }

    public Integer getVirtualUser() {
        return virtualUser;
    }

    public void setVirtualUser(Integer virtualUser) {
        this.virtualUser = virtualUser;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getTestResults() {
        return testResults;
    }

    public void setTestResults(String testResults) {
        this.testResults = testResults;
    }

    public String getAllHeaders() {
        return allHeaders;
    }

    public void setAllHeaders(String allHeaders) {
        this.allHeaders = allHeaders;
    }
}
