
package com.annuletconsulting.loadtest.postman.model.json;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "type",
    "exec"
})
public class Script {

    @JsonProperty("type")
    private String type;
    @JsonProperty("exec")
    private List<String> exec = new ArrayList<String>();

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The exec
     */
    @JsonProperty("exec")
    public List<String> getExec() {
        return exec;
    }

    /**
     * 
     * @param exec
     *     The exec
     */
    @JsonProperty("exec")
    public void setExec(List<String> exec) {
        this.exec = exec;
    }

}
