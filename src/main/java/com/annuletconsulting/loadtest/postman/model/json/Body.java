
package com.annuletconsulting.loadtest.postman.model.json;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mode",
    "urlencoded",
    "formdata",
    "file"
})
public class Body {

    @JsonProperty("formdata")
    private List<Value> formdata;

    @JsonProperty("urlencoded")
    private List<Value> urlencoded;

    @JsonProperty("mode")
    private String mode;

    @JsonProperty("file")
    @Valid
    private File file;

    @JsonProperty("formdata")
    public List<Value> getFormdata() {
        return formdata;
    }

    @JsonProperty("formdata")
    public void setFormdata(List<Value> formdata) {
        this.formdata = formdata;
    }

    @JsonProperty("urlencoded")
    public List<Value> getUrlencoded() {
        return urlencoded;
    }

    @JsonProperty("urlencoded")
    public void setUrlencoded(List<Value> urlencoded) {
        this.urlencoded = urlencoded;
    }

    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * 
     * @return
     *     The file
     */
    @JsonProperty("file")
    public File getFile() {
        return file;
    }

    /**
     * 
     * @param file
     *     The file
     */
    @JsonProperty("file")
    public void setFile(File file) {
        this.file = file;
    }

}
