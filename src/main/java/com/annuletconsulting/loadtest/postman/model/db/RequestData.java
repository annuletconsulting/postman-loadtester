package com.annuletconsulting.loadtest.postman.model.db;

import com.annuletconsulting.loadtest.postman.model.json.Value;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.http.HttpStatus;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by walt on 2/22/17.
 */
@Entity
@Table(name = "request_data")
public class RequestData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="run_id", nullable = false)
    private TestRun testRun;

    @JsonInclude
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "requestData")
    private List<ResponseData> responseDataList;

    @Column(nullable = false)
    private String url;

    @JsonInclude
    @Lob
    @Column(nullable = false)
    private String headerString;

    @JsonIgnore
    @Transient
    private ConcurrentMap<String, String> headerMap;

    @Column(nullable = false)
    private String method;

    @Column(nullable = false)
    private String folder;

    @Column(nullable = false)
    private String testName;

    @Column
    private String formDataString;

    @Column
    private String file;

    @Lob
    @Column
    private String testScript;

    @Transient
    private List<Value> formData;

    public RequestData() {
        // default constructor
    }

    @Transient
    @JsonInclude
    public boolean isAllSuccess() {
        for (ResponseData data : getResponseDataList()) {
            if (data.getHttpStatusCode() != HttpStatus.SC_OK) {
                return false;
            }
            if (testRun.isResponseTested() && ( data.getTestResults() == null ||
                    !data.getTestResults().contains("PASS") ||
                    data.getTestResults().contains("FAIL") ) ) {
                return false;
            }
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHeaderString() {
        return headerString;
    }

    public void setHeaderString(String headerString) {
        this.headerString = headerString;

        headerMap.clear();
        for (String headerEntry : headerString.split("\n")) {
            String[] splitEntry = headerEntry.split("=");
            headerMap.put(splitEntry[0], splitEntry[1]);
        }
    }

    public ConcurrentMap<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(ConcurrentMap<String, String> header) {
        this.headerMap = header;

        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry entry: headerMap.entrySet()) {
            stringBuilder.append(entry.getKey()).append('=').append(entry.getValue()).append('\n');
        }
        headerString = stringBuilder.toString().substring(0, stringBuilder.length()-1);
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public TestRun getTestRun() {
        return testRun;
    }

    public void setTestRun(TestRun testRun) {
        this.testRun = testRun;
    }

    public List<ResponseData> getResponseDataList() {
        return responseDataList;
    }

    public void setResponseDataList(List<ResponseData> responseDataList) {
        this.responseDataList = responseDataList;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getFormDataString() {
        return formDataString;
    }

    public void setFormDataString(String formDataString) {
        this.formDataString = formDataString;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getTestScript() {
        return testScript;
    }

    public void setTestScript(String testScript) {
        this.testScript = testScript;
    }

    public void setFormData(List<Value> formData) {
        this.formData = formData;
        if (null != formData) {
            StringBuilder builder = new StringBuilder();
            for (Value value : formData) {
                if (value.isEnabled()) {
                    builder.append(value.getKey()).append('=').append(value.getValue()).append(';');
                }
            }
            this.formDataString = builder.toString();
        }
    }

    public List<Value> getFormData() {
        return formData;
    }
}
