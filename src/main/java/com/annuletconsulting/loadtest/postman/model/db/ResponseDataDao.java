package com.annuletconsulting.loadtest.postman.model.db;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by walt on 2/25/17.
 */
@Transactional
public interface ResponseDataDao extends CrudRepository<ResponseData, Long> {
    ResponseData getByTestRun(TestRun testRun);
    List<ResponseData> getByRequestData(RequestData requestData);
}
