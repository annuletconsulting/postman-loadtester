package com.annuletconsulting.loadtest.postman.model.json.wrapper;

import com.annuletconsulting.loadtest.postman.model.db.TestRun;
import java.util.Date;

/**
 * Created by walt on 2/26/17.
 */
public class TestRunSummary {
    private Long id;
    private String collection;
    private String environment;
    private Integer virtualUsers;
    private Integer callsPerTest;
    private Integer testRepititions;
    private Date startTime;
    private Boolean complete;
    private Stats stats;

    public TestRunSummary(TestRun testRun) {
        id = testRun.getId();
        collection = testRun.getCollection();
        environment = testRun.getEnvironment();
        virtualUsers = testRun.getVirtualUsers();
        callsPerTest = testRun.getCallsPerTest();
        testRepititions = testRun.getTestRepititions();
        startTime = testRun.getStartTime();
        complete = testRun.isComplete();
        stats = testRun.getStats();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Integer getVirtualUsers() {
        return virtualUsers;
    }

    public void setVirtualUsers(Integer virtualUsers) {
        this.virtualUsers = virtualUsers;
    }

    public Integer getCallsPerTest() {
        return callsPerTest;
    }

    public void setCallsPerTest(Integer callsPerTest) {
        this.callsPerTest = callsPerTest;
    }

    public Integer getTestRepititions() {
        return testRepititions;
    }

    public void setTestRepititions(Integer testRepititions) {
        this.testRepititions = testRepititions;
    }

    public Date getStartTime() {
        return (Date) startTime.clone();
    }

    public void setStartTime(Date startTime) {
        this.startTime = (Date) startTime.clone();
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }
}
