package com.annuletconsulting.loadtest.postman.model.db;

import com.annuletconsulting.loadtest.postman.model.json.wrapper.Stats;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.http.HttpStatus;

import javax.persistence.*;
import java.util.*;

/**
 * Created by walt on 2/24/17.
 */
@Entity
@Table(name = "run")
public class TestRun {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String collection;

    @Column(nullable = false)
    private String environment;

    @Column(name = "virtual_users", nullable = false)
    private Integer virtualUsers;

    @Column(name = "calls_per_test")
    private Integer callsPerTest;

    @Column(name = "test_repititions", nullable = false)
    private Integer testRepititions;

    @Column(name = "start_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name = "save_response", nullable = false, columnDefinition = "TINYINT(1)")
    private boolean responsePersisted;

    @Column(name = "test_response", nullable = false, columnDefinition = "TINYINT(1)")
    private boolean responseTested;

    @JsonInclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "testRun")
    private List<RequestData> requestDataList;

    // These are mapped to their respective requests, so no need to include them again.
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "testRun")
    private List<ResponseData> responseDataList;

    public TestRun() {
        // Default Constructor
    }

    @Transient
    @JsonInclude
    public boolean isComplete() {
        if (null == getResponseDataList()) {
            return false;
        }
        return getResponseDataList().size() == virtualUsers * testRepititions * getCallsPerTest();
    }

    @Transient
    @JsonInclude
    public Stats getStats() {
        Stats stats = new Stats();
        stats.setTotalCalls(virtualUsers * testRepititions * getCallsPerTest());
        stats.setCompletedCalls(getResponseDataList().size());
        int failed = 0;
        int failedJSTests = 0;
        Date lastEndTime = null;
        for (ResponseData data : getResponseDataList()) {
            if (data.getHttpStatusCode() != HttpStatus.SC_OK) {
                failed++;
            }
            if (responseTested && ( data.getTestResults() == null ||
                                    !data.getTestResults().contains("PASS") ||
                                    data.getTestResults().contains("FAIL") ) ) {
                failedJSTests++;
            }
            if (null == lastEndTime || lastEndTime.before(data.getTestEnd().getTime())) {
                lastEndTime = data.getTestEnd().getTime();
            }
        }
        stats.setFailedCalls(failed);
        stats.setFailedJSTests(failedJSTests);
        stats.setRunTime(isComplete()?
                (null==lastEndTime?0:(lastEndTime.getTime()-getStartTime().getTime())):
                (Calendar.getInstance().getTimeInMillis()-getStartTime().getTime()));
        stats.setAvgTimePerCall(stats.getCompletedCalls()==0?0:(stats.getRunTime()/stats.getCompletedCalls()));
        return stats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Integer getVirtualUsers() {
        return virtualUsers;
    }

    public void setVirtualUsers(Integer virtualUsers) {
        this.virtualUsers = virtualUsers;
    }

    public Integer getCallsPerTest() {
        return null==callsPerTest ? 0 : callsPerTest;
    }

    public void setCallsPerTest(Integer callsPerTest) {
        this.callsPerTest = callsPerTest;
    }

    public Integer getTestRepititions() {
        return testRepititions;
    }

    public void setTestRepititions(Integer testRepititions) {
        this.testRepititions = testRepititions;
    }

    public List<RequestData> getRequestDataList() {
        return requestDataList;
    }

    public void setRequestDataList(List<RequestData> requestDataList) {
        this.requestDataList = requestDataList;
    }

    public List<ResponseData> getResponseDataList() {
        return responseDataList;
    }

    public void setResponseDataList(List<ResponseData> responseDataList) {
        this.responseDataList = responseDataList;
    }

    public void setStartTime(Date startTime) {
        this.startTime = (Date) startTime.clone();
    }

    public Date getStartTime() {
        return (Date) startTime.clone();
    }

    public boolean isResponsePersisted() {
        return responsePersisted;
    }

    public void setResponsePersisted(boolean responsePersisted) {
        this.responsePersisted = responsePersisted;
    }

    public boolean isResponseTested() {
        return responseTested;
    }

    public void setResponseTested(boolean responseTested) {
        this.responseTested = responseTested;
    }
}
