package com.annuletconsulting.loadtest.postman.model.json.wrapper;


/**
 * Created by walt on 2/26/17.
 */
public class Stats {
    private Integer totalCalls;
    private Integer completedCalls;
    private Integer failedCalls;
    private Integer failedJSTests;
    private long runTime;
    private long avgTimePerCall;

    public Integer getTotalCalls() {
        return totalCalls;
    }

    public void setTotalCalls(Integer totalCalls) {
        this.totalCalls = totalCalls;
    }

    public Integer getCompletedCalls() {
        return completedCalls;
    }

    public void setCompletedCalls(Integer completedCalls) {
        this.completedCalls = completedCalls;
    }

    public Integer getFailedCalls() {
        return failedCalls;
    }

    public void setFailedCalls(Integer failedCalls) {
        this.failedCalls = failedCalls;
    }

    public Integer getFailedJSTests() {
        return failedJSTests;
    }

    public void setFailedJSTests(Integer failedJSTests) {
        this.failedJSTests = failedJSTests;
    }

    public long getRunTime() {
        return runTime;
    }

    public void setRunTime(long runTime) {
        this.runTime = runTime;
    }

    public long getAvgTimePerCall() {
        return avgTimePerCall;
    }

    public void setAvgTimePerCall(long avgTimePerCall) {
        this.avgTimePerCall = avgTimePerCall;
    }
}
