
package com.annuletconsulting.loadtest.postman.model.json;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "src"
})
public class File {

    @JsonProperty("src")
    private String src;

    /**
     * 
     * @return
     *     The src
     */
    @JsonProperty("src")
    public String getSrc() {
        return src;
    }

    /**
     * 
     * @param src
     *     The src
     */
    @JsonProperty("src")
    public void setSrc(String src) {
        this.src = src;
    }

}
