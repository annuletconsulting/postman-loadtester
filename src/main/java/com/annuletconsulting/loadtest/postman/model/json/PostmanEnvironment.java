
package com.annuletconsulting.loadtest.postman.model.json;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "name",
    "values",
    "timestamp",
    "_postman_variable_scope",
    "_postman_exported_at",
    "_postman_exported_using"
})
public class PostmanEnvironment {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("values")
    private List<Value> values = new ArrayList<Value>();
    @JsonProperty("timestamp")
    private long timestamp;
    @JsonProperty("_postman_variable_scope")
    private String PostmanVariableScope;
    @JsonProperty("_postman_exported_at")
    private String PostmanExportedAt;
    @JsonProperty("_postman_exported_using")
    private String PostmanExportedUsing;

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The values
     */
    @JsonProperty("values")
    public List<Value> getValues() {
        return values;
    }

    /**
     * 
     * @param values
     *     The values
     */
    @JsonProperty("values")
    public void setValues(List<Value> values) {
        this.values = values;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    @JsonProperty("timestamp")
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    @JsonProperty("timestamp")
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 
     * @return
     *     The PostmanVariableScope
     */
    @JsonProperty("_postman_variable_scope")
    public String getPostmanVariableScope() {
        return PostmanVariableScope;
    }

    /**
     * 
     * @param PostmanVariableScope
     *     The _postman_variable_scope
     */
    @JsonProperty("_postman_variable_scope")
    public void setPostmanVariableScope(String PostmanVariableScope) {
        this.PostmanVariableScope = PostmanVariableScope;
    }

    /**
     * 
     * @return
     *     The PostmanExportedAt
     */
    @JsonProperty("_postman_exported_at")
    public String getPostmanExportedAt() {
        return PostmanExportedAt;
    }

    /**
     * 
     * @param PostmanExportedAt
     *     The _postman_exported_at
     */
    @JsonProperty("_postman_exported_at")
    public void setPostmanExportedAt(String PostmanExportedAt) {
        this.PostmanExportedAt = PostmanExportedAt;
    }

    /**
     * 
     * @return
     *     The PostmanExportedUsing
     */
    @JsonProperty("_postman_exported_using")
    public String getPostmanExportedUsing() {
        return PostmanExportedUsing;
    }

    /**
     * 
     * @param PostmanExportedUsing
     *     The _postman_exported_using
     */
    @JsonProperty("_postman_exported_using")
    public void setPostmanExportedUsing(String PostmanExportedUsing) {
        this.PostmanExportedUsing = PostmanExportedUsing;
    }

}
