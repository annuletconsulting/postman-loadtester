
package com.annuletconsulting.loadtest.postman.model.json;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "name",
    "description",
    "event",
    "request",
    "response",
    "item"
})
public class Item {

    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("item")
    private List<Item_> item = new ArrayList<Item_>();
    @JsonProperty("event")
    @Valid
    private List<com.annuletconsulting.loadtest.postman.model.json.Event> event = new ArrayList<com.annuletconsulting.loadtest.postman.model.json.Event>();
    @JsonProperty("request")
    @Valid
    private Request request;
    @JsonProperty("response")
    @Valid
    private List<Object> response = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The item
     */
    @JsonProperty("item")
    public List<Item_> getItem() {
        return item;
    }

    /**
     * 
     * @param item
     *     The item
     */
    @JsonProperty("item")
    public void setItem(List<Item_> item) {
        this.item = item;
    }

    /**
     *
     * @return
     *     The event
     */
    @JsonProperty("event")
    public List<com.annuletconsulting.loadtest.postman.model.json.Event> getEvent() {
        return event;
    }

    /**
     *
     * @param event
     *     The event
     */
    @JsonProperty("event")
    public void setEvent(List<com.annuletconsulting.loadtest.postman.model.json.Event> event) {
        this.event = event;
    }

    /**
     *
     * @return
     *     The request
     */
    @JsonProperty("request")
    public Request getRequest() {
        return request;
    }

    /**
     *
     * @param request
     *     The request
     */
    @JsonProperty("request")
    public void setRequest(Request request) {
        this.request = request;
    }

    /**
     *
     * @return
     *     The response
     */
    @JsonProperty("response")
    public List<Object> getResponse() {
        return response;
    }

    /**
     *
     * @param response
     *     The response
     */
    @JsonProperty("response")
    public void setResponse(List<Object> response) {
        this.response = response;
    }

}
