package com.annuletconsulting.loadtest.postman.controller;

import com.annuletconsulting.loadtest.postman.model.db.*;
import com.annuletconsulting.loadtest.postman.model.json.PostmanCollection;
import com.annuletconsulting.loadtest.postman.model.json.wrapper.TestRunSummary;
import com.annuletconsulting.loadtest.postman.model.json.PostmanEnvironment;
import com.annuletconsulting.loadtest.postman.service.runner.HttpTestRunner;
import com.annuletconsulting.loadtest.postman.util.PostmanUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.ws.WebServiceException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by walt on 2/25/17.
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestRunDao testRunDao;

    @Autowired
    private RequestDataDao requestDataDao;

    @Autowired
    private ResponseDataDao responseDataDao;

    public TestController() {
        // Default Constructor
    }

    @GetMapping
    public Iterable<TestRunSummary> getTestRuns() {
        List<TestRunSummary> summaryList = new ArrayList<>();
        for (TestRun testRun : testRunDao.findAll()) {
            summaryList.add(new TestRunSummary(testRun));
        }
        return summaryList;
    }

    @GetMapping("/{testRunId}")
    public TestRun getTestRun(@PathVariable Long testRunId) {
        return testRunDao.findOne(testRunId);
    }

    @PostMapping
    public Long startLoadTest(@RequestParam("postmanFile") MultipartFile postmanFile,
                              @RequestParam("envFile") MultipartFile envFile,
                              @RequestParam("virtualUsers") Integer virtualUsers,
                              @RequestParam("testRepetitions") Integer testRepetitions,
                              @RequestParam("persistResponse") String persistResponse,
                              @RequestParam("testResponse") String testResponse) {
        TestRun testRun = new TestRun();
        try {
            ObjectMapper mapper = new ObjectMapper();

            testRun.setCollection(postmanFile.getOriginalFilename());
            testRun.setEnvironment(envFile.getOriginalFilename());
            testRun.setTestRepititions(testRepetitions);
            testRun.setVirtualUsers(virtualUsers);
            testRun.setStartTime(Calendar.getInstance().getTime());

            boolean isTested = "true".equalsIgnoreCase(testResponse);
            testRun.setResponsePersisted(isTested || "true".equalsIgnoreCase(persistResponse));
            testRun.setResponseTested(isTested);

            testRunDao.save(testRun);

            List<RequestData> requestData = PostmanUtil.getRequestDataFromCollection(requestDataDao, testRun,
                    mapper.readValue(postmanFile.getInputStream(), PostmanCollection.class),
                    mapper.readValue(envFile.getInputStream(), PostmanEnvironment.class));

            // Add Call size and resave
            testRun.setCallsPerTest(requestData.size());
            testRunDao.save(testRun);

            if (testRun.getCallsPerTest() == 0) {
                throw new WebServiceException("No Requests Found!");
            }
            ExecutorService executor = Executors.newFixedThreadPool(virtualUsers);
            for (int i = 1; i <= virtualUsers; i++) {
                executor.execute(new HttpTestRunner(i, testRun, requestData, testRepetitions, responseDataDao));
            }
            executor.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebServiceException(e);
        }
        return testRun.getId();
    }
}
