package com.annuletconsulting.loadtest.postman.util;

import org.apache.http.*;
import org.apache.http.params.HttpParams;
import java.util.Locale;

/**
 * Created by walt on 4/6/17.
 */
public class HttpExceptionResponse implements HttpResponse {

    private final Exception exception;
    private final int major;
    private final int minor;
    private String protocol;
    private static final int STATUS_CODE = 999;
    private StatusLine statusLine = new StatusLine() {
        @Override
        public ProtocolVersion getProtocolVersion() {
            return new ProtocolVersion(protocol, major, minor);
        }

        @Override
        public int getStatusCode() {
            return STATUS_CODE;
        }

        @Override
        public String getReasonPhrase() {
            return exception.getLocalizedMessage();
        }
    };

    public HttpExceptionResponse(Exception exception, String protocol, int major, int minor) {
        super();
        this.exception = exception;
        this.protocol = protocol;
        this.major = major;
        this.minor = minor;
    }

    @Override
    public StatusLine getStatusLine() {
        return statusLine;
    }

    @Override
    public void setStatusLine(StatusLine statusLine) {
        this.statusLine = statusLine;
    }

    @Override
    public void setStatusLine(ProtocolVersion protocolVersion, int i) {
        throw new IllegalStateException();
    }

    @Override
    public void setStatusLine(ProtocolVersion protocolVersion, int i, String s) {
        throw new IllegalStateException();
    }

    @Override
    public void setStatusCode(int i) {
        throw new IllegalStateException();
    }

    @Override
    public void setReasonPhrase(String s) {
        throw new IllegalStateException();
    }

    @Override
    public HttpEntity getEntity() {
        return null;
    }

    @Override
    public void setEntity(HttpEntity httpEntity) {
        throw new IllegalStateException();
    }

    @Override
    public Locale getLocale() {
        return null;
    }

    @Override
    public void setLocale(Locale locale) {
        throw new IllegalStateException();
    }

    @Override
    public ProtocolVersion getProtocolVersion() {
        return statusLine.getProtocolVersion();
    }

    @Override
    public boolean containsHeader(String s) {
        return false;
    }

    @Override
    public Header[] getHeaders(String s) {
        return new Header[0];
    }

    @Override
    public Header getFirstHeader(String s) {
        return null;
    }

    @Override
    public Header getLastHeader(String s) {
        return null;
    }

    @Override
    public Header[] getAllHeaders() {
        return new Header[0];
    }

    @Override
    public void addHeader(Header header) {
        throw new IllegalStateException();
    }

    @Override
    public void addHeader(String s, String s1) {
        throw new IllegalStateException();
    }

    @Override
    public void setHeader(Header header) {
        throw new IllegalStateException();
    }

    @Override
    public void setHeader(String s, String s1) {
        throw new IllegalStateException();
    }

    @Override
    public void setHeaders(Header[] headers) {
        throw new IllegalStateException();
    }

    @Override
    public void removeHeader(Header header) {
        throw new IllegalStateException();
    }

    @Override
    public void removeHeaders(String s) {
        throw new IllegalStateException();
    }

    @Override
    public HeaderIterator headerIterator() {
        return null;
    }

    @Override
    public HeaderIterator headerIterator(String s) {
        return null;
    }

    @Override
    public HttpParams getParams() {
        return null;
    }

    @Override
    public void setParams(HttpParams httpParams) {
        throw new IllegalStateException();
    }
}
