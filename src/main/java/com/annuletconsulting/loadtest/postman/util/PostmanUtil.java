package com.annuletconsulting.loadtest.postman.util;

import com.annuletconsulting.loadtest.postman.model.db.*;
import com.annuletconsulting.loadtest.postman.model.js.ResponseCode;
import com.annuletconsulting.loadtest.postman.model.json.*;

import javax.script.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by walt on 4/8/17.
 */
public final class PostmanUtil {
    private static final String PREFIX = "\\{\\{";
    private static final String SUFFIX = "\\}\\}";

    private PostmanUtil() {
        // No need to create instance of this.
    }

    /**
     * Runs the Postman JS tests on the stored Response Data, if there are tests and response data.
     *
     * @param requestDataList
     * @param responseDataDao
     */
    public static void runPostmanTests(List<RequestData> requestDataList, ResponseDataDao responseDataDao) {
        for (RequestData requestData : requestDataList) {
            List<ResponseData> responseDataList = responseDataDao.getByRequestData(requestData);
            if (null != requestData.getTestScript() && null != responseDataList) {
                for (ResponseData responseData : responseDataList) {
                    runPostmanTest(requestData, responseData, responseDataDao);
                }
            }
        }
    }

    private static void runPostmanTest(RequestData requestData, ResponseData responseData,
                                       ResponseDataDao responseDataDao) {
        StringBuilder result = new StringBuilder();

        ScriptEngine js = new ScriptEngineManager().getEngineByName("javascript");
        Bindings bindings = js.getBindings(ScriptContext.ENGINE_SCOPE);
        bindings.put("pmlt_result", result);
        bindings.put("responseCode", new ResponseCode(responseData.getHttpStatusCode(), "Not Yet Implemented"));
        bindings.put("responseBody", responseData.getResponseBody());
        bindings.put("postman", new HeaderUtil(responseData.getAllHeaders()));
        bindings.put("console", new LogUtil());
        bindings.put("formatUtil", new FormatUtil());
        try {
            // Setup JS Environment
            js.eval("var tests = [];");
            js.eval("function xml2Json(xmlString) { return formatUtil.xml2Json(xmlString); }");

            // Run Postman Tests
            js.eval(requestData.getTestScript());

            // Retrieve results
            js.eval("for (var i in tests) { pmlt_result.append( i + ':' + (tests[i]?\"PASS\":\"FAIL\") + ';'); }");
            responseData.setTestResults(result.toString());
        } catch (ScriptException se) {
            se.printStackTrace();
            responseData.setTestResults("Error running JS: "+se.getMessage());
        }
        responseDataDao.save(responseData);
    }

    /**
     * Creates a list of RequestData objects from the Postman Collection and Environment files.
     *
     * @param requestDataDao
     * @param testRun
     * @param postmanCollection
     * @param postmanEnvironment
     * @return
     */
    public static List<RequestData> getRequestDataFromCollection(RequestDataDao requestDataDao, TestRun testRun,
                                                                 PostmanCollection postmanCollection,
                                                                 PostmanEnvironment postmanEnvironment) {
        List<RequestData> list = new ArrayList<>();
        for (Item folder : postmanCollection.getItem()) {
            for (Item_ test : folder.getItem()) {
                RequestData requestData = new RequestData();

                fillRequestData(folder, requestData, test.getRequest(), testRun, test.getName(),
                        postmanEnvironment.getValues(), test.getEvent());

                requestDataDao.save(requestData);
                list.add(requestData);
            }
            if (null != folder.getRequest()) {
                RequestData requestData = new RequestData();

                fillRequestData(folder, requestData, folder.getRequest(), testRun, folder.getRequest().getDescription(),
                        postmanEnvironment.getValues(), folder.getEvent());

                requestDataDao.save(requestData);
                list.add(requestData);
            }
        }
        return list;
    }

    private static void fillRequestData(Item folder, RequestData requestData, Request request, TestRun testRun,
                                        String testName, List<Value> values, List<Event> events) {
        requestData.setUrl(fillUrl(request.getUrl(), values));
        requestData.setMethod(request.getMethod());
        requestData.setHeaderMap(fillHeader(request.getHeader(), values));
        requestData.setFormData("formdata".equals(request.getBody().getMode())?
                request.getBody().getFormdata():
                request.getBody().getUrlencoded());
        requestData.setFile("file".equals(request.getBody().getMode())?
                request.getBody().getFile().getSrc():null);
        requestData.setFolder(folder.getName());
        requestData.setTestName(testName);
        requestData.setTestRun(testRun);

        if (null != events) {
            requestData.setTestScript(getTestScript(events));
        }
    }

    /**
     * Extract the JS tests from the EventList.
     *
     * @param eventList
     * @return
     */
    private static String getTestScript(List<Event> eventList) {
        StringBuilder builder = new StringBuilder();
        for (Event event : eventList) {
            if ("test".equals(event.getListen())) {
                Script script = event.getScript();
                if ("text/javascript".equals(script.getType())) {
                    for (String exec : script.getExec()) {
                        builder.append(exec).append(' ');
                    }
                }
            }
        }
        return builder.toString();
    }

    private static ConcurrentHashMap<String, String> fillHeader(List<Header> headers, List<Value> envValues) {
        ConcurrentHashMap<String, String> list = new ConcurrentHashMap<>(headers.size());
        for (Header header : headers) {
            String key = header.getKey();
            String value = header.getValue();
            for (Value envValue : envValues) {
                key = key.replaceAll(PREFIX+envValue.getKey()+SUFFIX, envValue.getValue());
                value = value.replaceAll(PREFIX+envValue.getKey()+SUFFIX, envValue.getValue());
            }
            list.put(key, value);
        }
        return list;
    }

    private static String fillUrl(String origUrl, List<Value> values) {
        String url = origUrl;
        for (Value value : values) {
            url = url.replaceAll(PREFIX + value.getKey() + SUFFIX, value.getValue());
        }
        return url;
    }
}
