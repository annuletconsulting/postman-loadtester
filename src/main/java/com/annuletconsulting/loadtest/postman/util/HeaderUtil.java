package com.annuletconsulting.loadtest.postman.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by walt on 4/11/17.
 */
public class HeaderUtil {
    private Map<String, String> headers;
    private Map<String, String> globalVariable;
    private Map<String, String> environmentVariable;

    public HeaderUtil(String headerString) {
        Map<String, String> headerMap = new HashMap<>();
        if (headerString.length() > 0 ) {
            for (String header : headerString.split(";")) {
                String[] pair = header.split("=");
                headerMap.put(pair[0], pair[1]);
            }
        }
        this.headers = headerMap;
    }

    public String getResponseHeader(String key) {
        return headers.get(key);
    }

    public String getGlobalVariable(String name) {
        return globalVariable.get(name);
    }

    public void setGlobalVariable(String name, String value) {
        globalVariable.put(name, value);
    }

    public void clearGlobalVariable(String name) {
        globalVariable.remove(name);
    }

    public String getEnvironmentVariable(String name) {
        return environmentVariable.get(name);
    }

    public void setEnvironmentVariable(String name, String value) {
        environmentVariable.put(name, value);
    }

    public void clearEnvironmentVariable(String name) {
        environmentVariable.remove(name);
    }
}
