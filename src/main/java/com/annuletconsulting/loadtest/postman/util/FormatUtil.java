package com.annuletconsulting.loadtest.postman.util;

import org.json.JSONObject;
import org.json.XML;

/**
 * Created by walt on 4/11/17.
 */
public class FormatUtil {
    public JSONObject xml2Json(String xml) {
        if (null == xml) {
            return null;
        }
        return XML.toJSONObject(xml);
    }
}
