package com.annuletconsulting.loadtest.postman;

/**
 * Created by walter.moorhouse on 2/7/17.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class PostmanLoadTestApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PostmanLoadTestApplication.class, args);
    }
}
