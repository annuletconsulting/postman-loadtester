# ReadMe #

## Version History ##

| Version       | Notes                                                                                          |
|:-------------:|------------------------------------------------------------------------------------------------|
| 1.1.4         | First attempt at supporting xml2json() and other Postman built-in functions in load tests.     |
| 1.1.3         | Updated Collection model and updated UI to reflect a test fail if Postman JS Tests fail.       |
| 1.1.2         | Ability to run Postman tests on each set of response data after the load tests are complete.   |
| 1.1.1         | Ability to save Response Data from each load test call.                                        |
| 1.0.0-RELEASE | Load testing from a simple web interface, with basic reporting.                                |


## Background ##
I switched to Postman for API testing and have a really solid set of tests, with which I wanted
to do some load testing. I did not need all the tests to be ran, but I wanted it to hit all the endpoints 
and check that it was a 200, not an error. There does not appear to be a way to do that from within 
Postman itself like there is in SoapUI, so I did a quick search and found a company called 
[LoadImpact](https://loadimpact.com) that says they can run Postman tests. Unfortunately, the do not support 
importing the tests, you have to install a conversion tool on your computer and convert the exported 
Postman tests into their format and then upload them. (Which means it has to be done every time you make 
changes to the Postman tests. Not a great solution for my workflow. In addition, the tool crashed on every 
Postman collection I pointed it at, so I was not even able to evaluate their solution.)
Their site looks very cool and seems to do a lot, but I only needed to run Postman tests, 
not use any of the other features, so when I saw their [pricing page](https://loadimpact.com/pricing) 
I decided that it wasn't wise to pay $1300/month for something which I could bang out some Java code to 
do in a few hours. If you're in a similar situation, then this app is for you.

## Prerequisites ##
If you have a set of Postman tests and an environment file, Maven, and JDK8 or greater and you're ready to build.
Clone or download this code to your local computer and run: mvn package at the project root. The executable jar 
will be in the target folder after you run maven. You will need a MySQL database installed. See below for 
configuration instructions.

## Build/Run Information ##
Running Maven package will result in an executable jar:
$ mvn clean package

Now just copy the jar onto the server you want to use to load test.


## Configuration ##
Create a database in MySQL and create a user with full rights (it will need to create tables.) The system will 
automatically configure the database.

The Default database name is load_test, but you can change it in the application.properties file by altering
the below lines.

spring.datasource.url = jdbc:mysql://localhost:3306/load_test?useSSL=false  
spring.datasource.username = tester  
spring.datasource.password = Sup3rS3kritPassw0rd  

Also in the application.properties file, you can change the default web port from 8080 to another port by 
altering the below line:

server.port = 8080

## Suggested Deployment ##
You probably noticed that there is no authentication on this app. I plan to add it later, but for now, I just
wanted to get something that would work.  So, for a secure deployment with long term access to test data, I 
suggest you build a small server/VM with just MySQL on it, and then change the application.properties to point
to it. Build the jar, spin up a huge VM (Each test is a thread, bigger VMs support more simultaneous tests) 
on the same intranet as the MySQL server and don't expose it to the public. Run your load tests (test data is 
stored on the DB server) then destroy the big expensive VM.  When you are ready to do more load test, just
recreate the big VM and redeploy the jar.
